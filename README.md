# MyRestAssured_Framework_Project

Created the RestAssured Framework encapsulating all the below requirements:

1. To configure all the Rest API
>Execute

>Extract response (Used RestAssured Liabrary)

>Parse the response (Used JsonPath Class of RestAssured Liabrary)

>Validate the response (Used TestNG Liabrary)

2. Framework is capable of reusability by creating common methods and centralised configuration files.
In this framework we created the Repository package which contains Environment class and Request Body class.

>Environment Class contains common data like Header name,Header Value, Hostname and Different Resource of Rest API.

>RequestBody class contains all the request bodies of different rest api methods.

3. Created the common method package for trigger and fetch response. This package contains API Trigger class and Utility Class.

>API TRIGGER- This class contains all trigger methods for All the Rest API request.

>Utility Class- This class contains commonutility methods such as Log directory creation,Evidence file creation and Read & write data from the Excle file.

4. Framework has the runner package which contains seperate runner for the all Restapi methods like POST,PATCH,PUT,GET and Delete.

5. Seperate test case class for each API methods.

6. Framework is able to do data driven testing by using apache POI Liabrary.

7. Source of API's
  >https://reqres.in/


